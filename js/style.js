
/* Modernizr 2.8.3 (Custom Build) | MIT & BSD
 * Build: http://modernizr.com/download/#-cssanimations-shiv-cssclasses-prefixed-testprop-testallprops-domprefixes-load
 */
;window.Modernizr=function(a,b,c){function x(a){j.cssText=a}function y(a,b){return x(prefixes.join(a+";")+(b||""))}function z(a,b){return typeof a===b}function A(a,b){return!!~(""+a).indexOf(b)}function B(a,b){for(var d in a){var e=a[d];if(!A(e,"-")&&j[e]!==c)return b=="pfx"?e:!0}return!1}function C(a,b,d){for(var e in a){var f=b[a[e]];if(f!==c)return d===!1?a[e]:z(f,"function")?f.bind(d||b):f}return!1}function D(a,b,c){var d=a.charAt(0).toUpperCase()+a.slice(1),e=(a+" "+n.join(d+" ")+d).split(" ");return z(b,"string")||z(b,"undefined")?B(e,b):(e=(a+" "+o.join(d+" ")+d).split(" "),C(e,b,c))}var d="2.8.3",e={},f=!0,g=b.documentElement,h="modernizr",i=b.createElement(h),j=i.style,k,l={}.toString,m="Webkit Moz O ms",n=m.split(" "),o=m.toLowerCase().split(" "),p={},q={},r={},s=[],t=s.slice,u,v={}.hasOwnProperty,w;!z(v,"undefined")&&!z(v.call,"undefined")?w=function(a,b){return v.call(a,b)}:w=function(a,b){return b in a&&z(a.constructor.prototype[b],"undefined")},Function.prototype.bind||(Function.prototype.bind=function(b){var c=this;if(typeof c!="function")throw new TypeError;var d=t.call(arguments,1),e=function(){if(this instanceof e){var a=function(){};a.prototype=c.prototype;var f=new a,g=c.apply(f,d.concat(t.call(arguments)));return Object(g)===g?g:f}return c.apply(b,d.concat(t.call(arguments)))};return e}),p.cssanimations=function(){return D("animationName")};for(var E in p)w(p,E)&&(u=E.toLowerCase(),e[u]=p[E](),s.push((e[u]?"":"no-")+u));return e.addTest=function(a,b){if(typeof a=="object")for(var d in a)w(a,d)&&e.addTest(d,a[d]);else{a=a.toLowerCase();if(e[a]!==c)return e;b=typeof b=="function"?b():b,typeof f!="undefined"&&f&&(g.className+=" "+(b?"":"no-")+a),e[a]=b}return e},x(""),i=k=null,function(a,b){function l(a,b){var c=a.createElement("p"),d=a.getElementsByTagName("head")[0]||a.documentElement;return c.innerHTML="x<style>"+b+"</style>",d.insertBefore(c.lastChild,d.firstChild)}function m(){var a=s.elements;return typeof a=="string"?a.split(" "):a}function n(a){var b=j[a[h]];return b||(b={},i++,a[h]=i,j[i]=b),b}function o(a,c,d){c||(c=b);if(k)return c.createElement(a);d||(d=n(c));var g;return d.cache[a]?g=d.cache[a].cloneNode():f.test(a)?g=(d.cache[a]=d.createElem(a)).cloneNode():g=d.createElem(a),g.canHaveChildren&&!e.test(a)&&!g.tagUrn?d.frag.appendChild(g):g}function p(a,c){a||(a=b);if(k)return a.createDocumentFragment();c=c||n(a);var d=c.frag.cloneNode(),e=0,f=m(),g=f.length;for(;e<g;e++)d.createElement(f[e]);return d}function q(a,b){b.cache||(b.cache={},b.createElem=a.createElement,b.createFrag=a.createDocumentFragment,b.frag=b.createFrag()),a.createElement=function(c){return s.shivMethods?o(c,a,b):b.createElem(c)},a.createDocumentFragment=Function("h,f","return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&("+m().join().replace(/[\w\-]+/g,function(a){return b.createElem(a),b.frag.createElement(a),'c("'+a+'")'})+");return n}")(s,b.frag)}function r(a){a||(a=b);var c=n(a);return s.shivCSS&&!g&&!c.hasCSS&&(c.hasCSS=!!l(a,"article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}")),k||q(a,c),a}var c="3.7.0",d=a.html5||{},e=/^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,f=/^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,g,h="_html5shiv",i=0,j={},k;(function(){try{var a=b.createElement("a");a.innerHTML="<xyz></xyz>",g="hidden"in a,k=a.childNodes.length==1||function(){b.createElement("a");var a=b.createDocumentFragment();return typeof a.cloneNode=="undefined"||typeof a.createDocumentFragment=="undefined"||typeof a.createElement=="undefined"}()}catch(c){g=!0,k=!0}})();var s={elements:d.elements||"abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",version:c,shivCSS:d.shivCSS!==!1,supportsUnknownElements:k,shivMethods:d.shivMethods!==!1,type:"default",shivDocument:r,createElement:o,createDocumentFragment:p};a.html5=s,r(b)}(this,b),e._version=d,e._domPrefixes=o,e._cssomPrefixes=n,e.testProp=function(a){return B([a])},e.testAllProps=D,e.prefixed=function(a,b,c){return b?D(a,b,c):D(a,"pfx")},g.className=g.className.replace(/(^|\s)no-js(\s|$)/,"$1$2")+(f?" js "+s.join(" "):""),e}(this,this.document),function(a,b,c){function d(a){return"[object Function]"==o.call(a)}function e(a){return"string"==typeof a}function f(){}function g(a){return!a||"loaded"==a||"complete"==a||"uninitialized"==a}function h(){var a=p.shift();q=1,a?a.t?m(function(){("c"==a.t?B.injectCss:B.injectJs)(a.s,0,a.a,a.x,a.e,1)},0):(a(),h()):q=0}function i(a,c,d,e,f,i,j){function k(b){if(!o&&g(l.readyState)&&(u.r=o=1,!q&&h(),l.onload=l.onreadystatechange=null,b)){"img"!=a&&m(function(){t.removeChild(l)},50);for(var d in y[c])y[c].hasOwnProperty(d)&&y[c][d].onload()}}var j=j||B.errorTimeout,l=b.createElement(a),o=0,r=0,u={t:d,s:c,e:f,a:i,x:j};1===y[c]&&(r=1,y[c]=[]),"object"==a?l.data=c:(l.src=c,l.type=a),l.width=l.height="0",l.onerror=l.onload=l.onreadystatechange=function(){k.call(this,r)},p.splice(e,0,u),"img"!=a&&(r||2===y[c]?(t.insertBefore(l,s?null:n),m(k,j)):y[c].push(l))}function j(a,b,c,d,f){return q=0,b=b||"j",e(a)?i("c"==b?v:u,a,b,this.i++,c,d,f):(p.splice(this.i++,0,a),1==p.length&&h()),this}function k(){var a=B;return a.loader={load:j,i:0},a}var l=b.documentElement,m=a.setTimeout,n=b.getElementsByTagName("script")[0],o={}.toString,p=[],q=0,r="MozAppearance"in l.style,s=r&&!!b.createRange().compareNode,t=s?l:n.parentNode,l=a.opera&&"[object Opera]"==o.call(a.opera),l=!!b.attachEvent&&!l,u=r?"object":l?"script":"img",v=l?"script":u,w=Array.isArray||function(a){return"[object Array]"==o.call(a)},x=[],y={},z={timeout:function(a,b){return b.length&&(a.timeout=b[0]),a}},A,B;B=function(a){function b(a){var a=a.split("!"),b=x.length,c=a.pop(),d=a.length,c={url:c,origUrl:c,prefixes:a},e,f,g;for(f=0;f<d;f++)g=a[f].split("="),(e=z[g.shift()])&&(c=e(c,g));for(f=0;f<b;f++)c=x[f](c);return c}function g(a,e,f,g,h){var i=b(a),j=i.autoCallback;i.url.split(".").pop().split("?").shift(),i.bypass||(e&&(e=d(e)?e:e[a]||e[g]||e[a.split("/").pop().split("?")[0]]),i.instead?i.instead(a,e,f,g,h):(y[i.url]?i.noexec=!0:y[i.url]=1,f.load(i.url,i.forceCSS||!i.forceJS&&"css"==i.url.split(".").pop().split("?").shift()?"c":c,i.noexec,i.attrs,i.timeout),(d(e)||d(j))&&f.load(function(){k(),e&&e(i.origUrl,h,g),j&&j(i.origUrl,h,g),y[i.url]=2})))}function h(a,b){function c(a,c){if(a){if(e(a))c||(j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}),g(a,j,b,0,h);else if(Object(a)===a)for(n in m=function(){var b=0,c;for(c in a)a.hasOwnProperty(c)&&b++;return b}(),a)a.hasOwnProperty(n)&&(!c&&!--m&&(d(j)?j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}:j[n]=function(a){return function(){var b=[].slice.call(arguments);a&&a.apply(this,b),l()}}(k[n])),g(a[n],j,b,n,h))}else!c&&l()}var h=!!a.test,i=a.load||a.both,j=a.callback||f,k=j,l=a.complete||f,m,n;c(h?a.yep:a.nope,!!i),i&&c(i)}var i,j,l=this.yepnope.loader;if(e(a))g(a,0,l,0);else if(w(a))for(i=0;i<a.length;i++)j=a[i],e(j)?g(j,0,l,0):w(j)?B(j):Object(j)===j&&h(j,l);else Object(a)===a&&h(a,l)},B.addPrefix=function(a,b){z[a]=b},B.addFilter=function(a){x.push(a)},B.errorTimeout=1e4,null==b.readyState&&b.addEventListener&&(b.readyState="loading",b.addEventListener("DOMContentLoaded",A=function(){b.removeEventListener("DOMContentLoaded",A,0),b.readyState="complete"},0)),a.yepnope=k(),a.yepnope.executeStack=h,a.yepnope.injectJs=function(a,c,d,e,i,j){var k=b.createElement("script"),l,o,e=e||B.errorTimeout;k.src=a;for(o in d)k.setAttribute(o,d[o]);c=j?h:c||f,k.onreadystatechange=k.onload=function(){!l&&g(k.readyState)&&(l=1,c(),k.onload=k.onreadystatechange=null)},m(function(){l||(l=1,c(1))},e),i?k.onload():n.parentNode.insertBefore(k,n)},a.yepnope.injectCss=function(a,c,d,e,g,i){var e=b.createElement("link"),j,c=i?h:c||f;e.href=a,e.rel="stylesheet",e.type="text/css";for(j in d)e.setAttribute(j,d[j]);g||(n.parentNode.insertBefore(e,n),m(c,0))}}(this,document),Modernizr.load=function(){yepnope.apply(window,[].slice.call(arguments,0))};
/*!
 * classie - class helper functions
 * from bonzo https://github.com/ded/bonzo
 *
 * classie.has( elem, 'my-class' ) -> true/false
 * classie.add( elem, 'my-new-class' )
 * classie.remove( elem, 'my-unwanted-class' )
 * classie.toggle( elem, 'my-class' )
 */

/*jshint browser: true, strict: true, undef: true */
/*global define: false */

( function( window ) {

'use strict';

// class helper functions from bonzo https://github.com/ded/bonzo

function classReg( className ) {
  return new RegExp("(^|\\s+)" + className + "(\\s+|$)");
}

// classList support for class management
// altho to be fair, the api sucks because it won't accept multiple classes at once
var hasClass, addClass, removeClass;

if ( 'classList' in document.documentElement ) {
  hasClass = function( elem, c ) {
    return elem.classList.contains( c );
  };
  addClass = function( elem, c ) {
    elem.classList.add( c );
  };
  removeClass = function( elem, c ) {
    elem.classList.remove( c );
  };
}
else {
  hasClass = function( elem, c ) {
    return classReg( c ).test( elem.className );
  };
  addClass = function( elem, c ) {
    if ( !hasClass( elem, c ) ) {
      elem.className = elem.className + ' ' + c;
    }
  };
  removeClass = function( elem, c ) {
    elem.className = elem.className.replace( classReg( c ), ' ' );
  };
}

function toggleClass( elem, c ) {
  var fn = hasClass( elem, c ) ? removeClass : addClass;
  fn( elem, c );
}

var classie = {
  // full names
  hasClass: hasClass,
  addClass: addClass,
  removeClass: removeClass,
  toggleClass: toggleClass,
  // short names
  has: hasClass,
  add: addClass,
  remove: removeClass,
  toggle: toggleClass
};

// transport
if ( typeof define === 'function' && define.amd ) {
  // AMD
  define( classie );
} else {
  // browser global
  window.classie = classie;
}

})( window );
/*!
 * clipboard.js v1.5.5
 * https://zenorocha.github.io/clipboard.js
 *
 * Licensed MIT © Zeno Rocha
 */
!function(t){if("object"==typeof exports&&"undefined"!=typeof module)module.exports=t();else if("function"==typeof define&&define.amd)define([],t);else{var e;e="undefined"!=typeof window?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:this,e.Clipboard=t()}}(function(){var t,e,n;return function t(e,n,r){function o(a,c){if(!n[a]){if(!e[a]){var s="function"==typeof require&&require;if(!c&&s)return s(a,!0);if(i)return i(a,!0);var u=new Error("Cannot find module '"+a+"'");throw u.code="MODULE_NOT_FOUND",u}var l=n[a]={exports:{}};e[a][0].call(l.exports,function(t){var n=e[a][1][t];return o(n?n:t)},l,l.exports,t,e,n,r)}return n[a].exports}for(var i="function"==typeof require&&require,a=0;a<r.length;a++)o(r[a]);return o}({1:[function(t,e,n){var r=t("matches-selector");e.exports=function(t,e,n){for(var o=n?t:t.parentNode;o&&o!==document;){if(r(o,e))return o;o=o.parentNode}}},{"matches-selector":2}],2:[function(t,e,n){function r(t,e){if(i)return i.call(t,e);for(var n=t.parentNode.querySelectorAll(e),r=0;r<n.length;++r)if(n[r]==t)return!0;return!1}var o=Element.prototype,i=o.matchesSelector||o.webkitMatchesSelector||o.mozMatchesSelector||o.msMatchesSelector||o.oMatchesSelector;e.exports=r},{}],3:[function(t,e,n){function r(t,e,n,r){var i=o.apply(this,arguments);return t.addEventListener(n,i),{destroy:function(){t.removeEventListener(n,i)}}}function o(t,e,n,r){return function(n){n.delegateTarget=i(n.target,e,!0),n.delegateTarget&&r.call(t,n)}}var i=t("closest");e.exports=r},{closest:1}],4:[function(t,e,n){n.node=function(t){return void 0!==t&&t instanceof HTMLElement&&1===t.nodeType},n.nodeList=function(t){var e=Object.prototype.toString.call(t);return void 0!==t&&("[object NodeList]"===e||"[object HTMLCollection]"===e)&&"length"in t&&(0===t.length||n.node(t[0]))},n.string=function(t){return"string"==typeof t||t instanceof String},n.function=function(t){var e=Object.prototype.toString.call(t);return"[object Function]"===e}},{}],5:[function(t,e,n){function r(t,e,n){if(!t&&!e&&!n)throw new Error("Missing required arguments");if(!c.string(e))throw new TypeError("Second argument must be a String");if(!c.function(n))throw new TypeError("Third argument must be a Function");if(c.node(t))return o(t,e,n);if(c.nodeList(t))return i(t,e,n);if(c.string(t))return a(t,e,n);throw new TypeError("First argument must be a String, HTMLElement, HTMLCollection, or NodeList")}function o(t,e,n){return t.addEventListener(e,n),{destroy:function(){t.removeEventListener(e,n)}}}function i(t,e,n){return Array.prototype.forEach.call(t,function(t){t.addEventListener(e,n)}),{destroy:function(){Array.prototype.forEach.call(t,function(t){t.removeEventListener(e,n)})}}}function a(t,e,n){return s(document.body,t,e,n)}var c=t("./is"),s=t("delegate");e.exports=r},{"./is":4,delegate:3}],6:[function(t,e,n){function r(t){var e;if("INPUT"===t.nodeName||"TEXTAREA"===t.nodeName)t.focus(),t.setSelectionRange(0,t.value.length),e=t.value;else{t.hasAttribute("contenteditable")&&t.focus();var n=window.getSelection(),r=document.createRange();r.selectNodeContents(t),n.removeAllRanges(),n.addRange(r),e=n.toString()}return e}e.exports=r},{}],7:[function(t,e,n){function r(){}r.prototype={on:function(t,e,n){var r=this.e||(this.e={});return(r[t]||(r[t]=[])).push({fn:e,ctx:n}),this},once:function(t,e,n){function r(){o.off(t,r),e.apply(n,arguments)}var o=this;return r._=e,this.on(t,r,n)},emit:function(t){var e=[].slice.call(arguments,1),n=((this.e||(this.e={}))[t]||[]).slice(),r=0,o=n.length;for(r;o>r;r++)n[r].fn.apply(n[r].ctx,e);return this},off:function(t,e){var n=this.e||(this.e={}),r=n[t],o=[];if(r&&e)for(var i=0,a=r.length;a>i;i++)r[i].fn!==e&&r[i].fn._!==e&&o.push(r[i]);return o.length?n[t]=o:delete n[t],this}},e.exports=r},{}],8:[function(t,e,n){"use strict";function r(t){return t&&t.__esModule?t:{"default":t}}function o(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}n.__esModule=!0;var i=function(){function t(t,e){for(var n=0;n<e.length;n++){var r=e[n];r.enumerable=r.enumerable||!1,r.configurable=!0,"value"in r&&(r.writable=!0),Object.defineProperty(t,r.key,r)}}return function(e,n,r){return n&&t(e.prototype,n),r&&t(e,r),e}}(),a=t("select"),c=r(a),s=function(){function t(e){o(this,t),this.resolveOptions(e),this.initSelection()}return t.prototype.resolveOptions=function t(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0];this.action=e.action,this.emitter=e.emitter,this.target=e.target,this.text=e.text,this.trigger=e.trigger,this.selectedText=""},t.prototype.initSelection=function t(){if(this.text&&this.target)throw new Error('Multiple attributes declared, use either "target" or "text"');if(this.text)this.selectFake();else{if(!this.target)throw new Error('Missing required attributes, use either "target" or "text"');this.selectTarget()}},t.prototype.selectFake=function t(){var e=this;this.removeFake(),this.fakeHandler=document.body.addEventListener("click",function(){return e.removeFake()}),this.fakeElem=document.createElement("textarea"),this.fakeElem.style.position="absolute",this.fakeElem.style.left="-9999px",this.fakeElem.style.top=(window.pageYOffset||document.documentElement.scrollTop)+"px",this.fakeElem.setAttribute("readonly",""),this.fakeElem.value=this.text,document.body.appendChild(this.fakeElem),this.selectedText=c.default(this.fakeElem),this.copyText()},t.prototype.removeFake=function t(){this.fakeHandler&&(document.body.removeEventListener("click"),this.fakeHandler=null),this.fakeElem&&(document.body.removeChild(this.fakeElem),this.fakeElem=null)},t.prototype.selectTarget=function t(){this.selectedText=c.default(this.target),this.copyText()},t.prototype.copyText=function t(){var e=void 0;try{e=document.execCommand(this.action)}catch(n){e=!1}this.handleResult(e)},t.prototype.handleResult=function t(e){e?this.emitter.emit("success",{action:this.action,text:this.selectedText,trigger:this.trigger,clearSelection:this.clearSelection.bind(this)}):this.emitter.emit("error",{action:this.action,trigger:this.trigger,clearSelection:this.clearSelection.bind(this)})},t.prototype.clearSelection=function t(){this.target&&this.target.blur(),window.getSelection().removeAllRanges()},t.prototype.destroy=function t(){this.removeFake()},i(t,[{key:"action",set:function t(){var e=arguments.length<=0||void 0===arguments[0]?"copy":arguments[0];if(this._action=e,"copy"!==this._action&&"cut"!==this._action)throw new Error('Invalid "action" value, use either "copy" or "cut"')},get:function t(){return this._action}},{key:"target",set:function t(e){if(void 0!==e){if(!e||"object"!=typeof e||1!==e.nodeType)throw new Error('Invalid "target" value, use a valid Element');this._target=e}},get:function t(){return this._target}}]),t}();n.default=s,e.exports=n.default},{select:6}],9:[function(t,e,n){"use strict";function r(t){return t&&t.__esModule?t:{"default":t}}function o(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}function i(t,e){if("function"!=typeof e&&null!==e)throw new TypeError("Super expression must either be null or a function, not "+typeof e);t.prototype=Object.create(e&&e.prototype,{constructor:{value:t,enumerable:!1,writable:!0,configurable:!0}}),e&&(Object.setPrototypeOf?Object.setPrototypeOf(t,e):t.__proto__=e)}function a(t,e){var n="data-clipboard-"+t;if(e.hasAttribute(n))return e.getAttribute(n)}n.__esModule=!0;var c=t("./clipboard-action"),s=r(c),u=t("tiny-emitter"),l=r(u),f=t("good-listener"),d=r(f),h=function(t){function e(n,r){o(this,e),t.call(this),this.resolveOptions(r),this.listenClick(n)}return i(e,t),e.prototype.resolveOptions=function t(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0];this.action="function"==typeof e.action?e.action:this.defaultAction,this.target="function"==typeof e.target?e.target:this.defaultTarget,this.text="function"==typeof e.text?e.text:this.defaultText},e.prototype.listenClick=function t(e){var n=this;this.listener=d.default(e,"click",function(t){return n.onClick(t)})},e.prototype.onClick=function t(e){var n=e.delegateTarget||e.currentTarget;this.clipboardAction&&(this.clipboardAction=null),this.clipboardAction=new s.default({action:this.action(n),target:this.target(n),text:this.text(n),trigger:n,emitter:this})},e.prototype.defaultAction=function t(e){return a("action",e)},e.prototype.defaultTarget=function t(e){var n=a("target",e);return n?document.querySelector(n):void 0},e.prototype.defaultText=function t(e){return a("text",e)},e.prototype.destroy=function t(){this.listener.destroy(),this.clipboardAction&&(this.clipboardAction.destroy(),this.clipboardAction=null)},e}(l.default);n.default=h,e.exports=n.default},{"./clipboard-action":8,"good-listener":5,"tiny-emitter":7}]},{},[9])(9)});


(function() {
			[].slice.call(document.querySelectorAll('.nav')).forEach(function(nav) {
				var navItems = nav.querySelectorAll('.nav-item-link'),
					setCurrent = function(ev) {
						ev.preventDefault();
						var item = ev.target.parentNode; // li
						// return if already current
						if (classie.has(item, 'nav-item--current')) {
							return false;
						}
						// remove current
						classie.remove(nav.querySelector('.nav-item--current'), 'nav-item--current');
						// set current
						classie.add(item, 'nav-item--current');
					};
				[].slice.call(navItems).forEach(function(el) {
					el.addEventListener('click', setCurrent);
				});
			});
			[].slice.call(document.querySelectorAll('.link-copy')).forEach(function(link) {
				link.setAttribute('data-clipboard-text', location.protocol + '//' + location.host + location.pathname + '#' + link.parentNode.id);
				new Clipboard(link);
				link.addEventListener('click', function() {
					classie.add(link, 'link-copy--animate');
					setTimeout(function() {
						classie.remove(link, 'link-copy--animate');
					}, 300);
				});
			});
		})(window);
    /**
     * sidebarEffects.js v1.0.0
     * http://www.codrops.com
     *
     * Licensed under the MIT license.
     * http://www.opensource.org/licenses/mit-license.php
     *
     * Copyright 2013, Codrops
     * http://www.codrops.com
     */
     var SidebarnavEffects = (function() {

     	function hasParentClass( e, classname ) {
    		if(e === document) return false;
    		if( classie.has( e, classname ) ) {
    			return true;
    		}
    		return e.parentNode && hasParentClass( e.parentNode, classname );
    	}

    	// http://coveroverflow.com/a/11381730/989439
    	function mobilecheck() {
    		var check = false;
    		(function(a){if(/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
    		return check;
    	}

	function init() {

		var container = document.getElementById( 'st-container' ),
	            	reset = document.getElementById( 'closenav' ),
			buttons = Array.prototype.slice.call( document.querySelectorAll( '#st-trigger-effects > button' ) ),
			// event type (if mobile use touch events)
			eventtype = mobilecheck() ? 'touchstart' : 'click',
			resetnav = function() {
				classie.remove( container, 'st-nav-open' );
			},
			bodyClickFn = function(evt) {
				if( !hasParentClass( evt.target, 'st-nav' ) ) {
					resetnav();
					document.removeEventListener( eventtype, bodyClickFn );
				}
			},
			resetClickFn = function(evt) {
				if (evt.target == reset) {
					resetnav();
					document.removeEventListener(eventtype, bodyClickFn);
				}
			};

		buttons.forEach( function( el, i ) {
			var effect = el.getAttribute( 'data-effect' );

			el.addEventListener( eventtype, function( ev ) {
				ev.stopPropagation();
				ev.preventDefault();
				container.className = 'st-container'; // clear
				classie.add( container, effect );
				setTimeout( function() {
					classie.add( container, 'st-nav-open' );
				}, 25 );
				document.addEventListener( eventtype, bodyClickFn );
				document.addEventListener( eventtype, resetClickFn );
			});
		} );

	}

	init();

})();
(function() {
				// trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
				if (!String.prototype.trim) {
					(function() {
						// Make sure we trim BOM and NBSP
						var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
						String.prototype.trim = function() {
							return this.replace(rtrim, '');
						};
					})();
				}
				[].slice.call( document.querySelectorAll( 'input.input-field' ) ).forEach( function( inputEl ) {
					// in case the input is already filled..
					if( inputEl.value.trim() !== '' ) {
						classie.add( inputEl.parentNode, 'input--filled' );
					}
					// events:
					inputEl.addEventListener( 'focus', onInputFocus );
					inputEl.addEventListener( 'blur', onInputBlur );
				} );
				function onInputFocus( ev ) {
					classie.add( ev.target.parentNode, 'input--filled' );
				}
				function onInputBlur( ev ) {
					if( ev.target.value.trim() === '' ) {
						classie.remove( ev.target.parentNode, 'input--filled' );
					}
				}
			})();
(function() {
				// http://stackoverflow.com/a/11381730/989439
				function mobilecheck() {
					var check = false;
					(function(a){if(/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
					return check;
				}
				var support = { animations : Modernizr.cssanimations },
					animEndEventNames = { 'WebkitAnimation' : 'webkitAnimationEnd', 'OAnimation' : 'oAnimationEnd', 'msAnimation' : 'MSAnimationEnd', 'animation' : 'animationend' },
					animEndEventName = animEndEventNames[ Modernizr.prefixed( 'animation' ) ],
					onEndAnimation = function( el, callback ) {
						var onEndCallbackFn = function( ev ) {
							if( support.animations ) {
								if( ev.target != this ) return;
								this.removeEventListener( animEndEventName, onEndCallbackFn );
							}
							if( callback && typeof callback === 'function' ) { callback.call(); }
						};
						if( support.animations ) {
							el.addEventListener( animEndEventName, onEndCallbackFn );
						}
						else {
							onEndCallbackFn();
						}
					},
					eventtype = mobilecheck() ? 'touchstart' : 'click';
				[].slice.call( document.querySelectorAll( '.icon-btn' ) ).forEach( function( el ) {
					el.addEventListener( eventtype, function( ev ) {
						classie.add( el, 'icon-btn--click' );
						onEndAnimation( classie.has( el, 'icon-btn--complex' ) ? el.querySelector( '.icon-btn__helper' ) : el, function() {
							classie.remove( el, 'icon-btn--click' );
						} );
					} );
				} );
			})();
      ;( function() {

      function factory(classie) {

      	'use strict';

      	/**
      	 * based on from https://github.com/inuyaksa/jquery.nicescroll/blob/master/jquery.nicescroll.js
      	 */
      	function hasParent( e, p ) {
      		if (!e) return false;
      		var el = e.target||e.srcElement||e||false;
      		while (el && el != p) {
      			el = el.parentNode||false;
      		}
      		return (el!==false);
      	}

      	/**
      	 * extend obj function
      	 */
      	function extend( a, b ) {
      		for( var key in b ) {
      			if( b.hasOwnProperty( key ) ) {
      				a[key] = b[key];
      			}
      		}
      		return a;
      	}

      	/**
      	 * SelectFx function
      	 */
      	function SelectFx( el, options ) {
      		this.el = el;
      		this.options = extend( {}, this.options );
      		extend( this.options, options );
      		this._init();
      	}

      	/**
      	 * SelectFx options
      	 */
      	SelectFx.prototype.options = {
      		// if true all the links will open in a new tab.
      		// if we want to be redirected when we click an option, we need to define a data-link attr on the option of the native select element
      		newTab : true,
      		// when opening the select element, the default placeholder (if any) is shown
      		stickyPlaceholder : true,
      		// callback when changing the value
      		onChange : function( val ) { return false; }
      	};

      	/**
      	 * init function
      	 * initialize and cache some vars
      	 */
      	SelectFx.prototype._init = function() {
      		// check if we are using a placeholder for the native select box
      		// we assume the placeholder is disabled and selected by default
      		var selectedOpt = this.el.options[this.el.selectedIndex];
      		this.hasDefaultPlaceholder = selectedOpt && selectedOpt.disabled;

      		// get selected option (either the first option with attr selected or just the first option)
      		this.selectedOpt = selectedOpt || this.el.querySelector( 'option' );

      		// create structure
      		this._createSelectEl();

      		// all options
      		this.selOpts = [].slice.call( this.selEl.querySelectorAll( 'li[data-option]' ) );

      		// total options
      		this.selOptsCount = this.selOpts.length;

      		// current index
      		this.current = this.selOpts.indexOf( this.selEl.querySelector( 'li.cs-selected' ) ) || -1;

      		// placeholder elem
      		this.selPlaceholder = this.selEl.querySelector( 'span.cs-placeholder' );

      		// init events
      		this._initEvents();
      	};

      	/**
      	 * creates the structure for the select element
      	 */
      	SelectFx.prototype._createSelectEl = function() {
      		var self = this, options = '', createOptionHTML = function(el) {
      			var optclass = '', classes = '', link = '';

      			if( el.selectedOpt && !this.foundSelected && !this.hasDefaultPlaceholder ) {
      				classes += 'cs-selected ';
      				this.foundSelected = true;
      			}
      			// extra classes
      			if( el.getAttribute( 'data-class' ) ) {
      				classes += el.getAttribute( 'data-class' );
      			}
      			// link options
      			if( el.getAttribute( 'data-link' ) ) {
      				link = 'data-link=' + el.getAttribute( 'data-link' );
      			}

      			if( classes !== '' ) {
      				optclass = 'class="' + classes + '" ';
      			}

      			var extraAttributes = '';

      			[].forEach.call(el.attributes, function(attr) {
      				var name = attr['name'];

      				if(name.indexOf('data-') + ['data-option', 'data-value'].indexOf(name) == -1){
      					extraAttributes += name + "='" + attr['value'] + "' ";
      				}
      			} );

      			return '<li ' + optclass + link + extraAttributes + ' data-option data-value="' + el.value + '"><span>' + el.textContent + '</span></li>';
      		};

      		[].slice.call( this.el.children ).forEach( function(el) {
      			if( el.disabled ) { return; }

      			var tag = el.tagName.toLowerCase();

      			if( tag === 'option' ) {
      				options += createOptionHTML(el);
      			}
      			else if( tag === 'optgroup' ) {
      				options += '<li class="cs-optgroup"><span>' + el.label + '</span><ul>';
      				[].slice.call( el.children ).forEach( function(opt) {
      					options += createOptionHTML(opt);
      				} );
      				options += '</ul></li>';
      			}
      		} );

      		var opts_el = '<div class="cs-options"><ul>' + options + '</ul></div>';
      		this.selEl = document.createElement( 'div' );
      		this.selEl.className = this.el.className;
      		this.selEl.tabIndex = this.el.tabIndex;
      		this.selEl.innerHTML = '<span class="cs-placeholder">' + this.selectedOpt.textContent + '</span>' + opts_el;
      		this.el.parentNode.appendChild( this.selEl );
      		this.selEl.appendChild( this.el );
      	};

      	/**
      	 * initialize the events
      	 */
      	SelectFx.prototype._initEvents = function() {
      		var self = this;

      		// open/close select
      		this.selPlaceholder.addEventListener( 'click', function() {
      			self._toggleSelect();
      		} );

      		// clicking the options
      		this.selOpts.forEach( function(opt, idx) {
      			opt.addEventListener( 'click', function() {
      				self.current = idx;
      				self._changeOption();
      				// close select elem
      				self._toggleSelect();
      			} );
      		} );

      		// close the select element if the target it´s not the select element or one of its descendants..
      		document.addEventListener( 'click', function(ev) {
      			var target = ev.target;
      			if( self._isOpen() && target !== self.selEl && !hasParent( target, self.selEl ) ) {
      				self._toggleSelect();
      			}
      		} );

      		// keyboard navigation events
      		this.selEl.addEventListener( 'keydown', function( ev ) {
      			var keyCode = ev.keyCode || ev.which;

      			switch (keyCode) {
      				// up key
      				case 38:
      					ev.preventDefault();
      					self._navigateOpts('prev');
      					break;
      				// down key
      				case 40:
      					ev.preventDefault();
      					self._navigateOpts('next');
      					break;
      				// space key
      				case 32:
      					ev.preventDefault();
      					if( self._isOpen() && typeof self.preSelCurrent != 'undefined' && self.preSelCurrent !== -1 ) {
      						self._changeOption();
      					}
      					self._toggleSelect();
      					break;
      				// enter key
      				case 13:
      					ev.preventDefault();
      					if( self._isOpen() && typeof self.preSelCurrent != 'undefined' && self.preSelCurrent !== -1 ) {
      						self._changeOption();
      						self._toggleSelect();
      					}
      					break;
      				// esc key
      				case 27:
      					ev.preventDefault();
      					if( self._isOpen() ) {
      						self._toggleSelect();
      					}
      					break;
      			}
      		} );
      	};

      	/**
      	 * navigate with up/dpwn keys
      	 */
      	SelectFx.prototype._navigateOpts = function(dir) {
      		if( !this._isOpen() ) {
      			this._toggleSelect();
      		}

      		var tmpcurrent = typeof this.preSelCurrent != 'undefined' && this.preSelCurrent !== -1 ? this.preSelCurrent : this.current;

      		if( dir === 'prev' && tmpcurrent > 0 || dir === 'next' && tmpcurrent < this.selOptsCount - 1 ) {
      			// save pre selected current - if we click on option, or press enter, or press space this is going to be the index of the current option
      			this.preSelCurrent = dir === 'next' ? tmpcurrent + 1 : tmpcurrent - 1;
      			// remove focus class if any..
      			this._removeFocus();
      			// add class focus - track which option we are navigating
      			classie.add( this.selOpts[this.preSelCurrent], 'cs-focus' );
      		}
      	};

      	/**
      	 * open/close select
      	 * when opened show the default placeholder if any
      	 */
      	SelectFx.prototype._toggleSelect = function() {
      		// remove focus class if any..
      		this._removeFocus();

      		if( this._isOpen() ) {
      			if( this.current !== -1 ) {
      				// update placeholder text
      				this.selPlaceholder.textContent = this.selOpts[ this.current ].textContent;
      			}
      			classie.remove( this.selEl, 'cs-active' );
      		}
      		else {
      			if( this.hasDefaultPlaceholder && this.options.stickyPlaceholder ) {
      				// everytime we open we wanna see the default placeholder text
      				this.selPlaceholder.textContent = this.selectedOpt.textContent;
      			}
      			classie.add( this.selEl, 'cs-active' );
      		}
      	};

      	/**
      	 * change option - the new value is set
      	 */
      	SelectFx.prototype._changeOption = function() {
      		// if pre selected current (if we navigate with the keyboard)...
      		if( typeof this.preSelCurrent != 'undefined' && this.preSelCurrent !== -1 ) {
      			this.current = this.preSelCurrent;
      			this.preSelCurrent = -1;
      		}

      		// current option
      		var opt = this.selOpts[ this.current ];

      		// update current selected value
      		this.selPlaceholder.textContent = opt.textContent;

      		// change native select element´s value
      		this.el.value = opt.getAttribute( 'data-value' );

      		// remove class cs-selected from old selected option and add it to current selected option
      		var oldOpt = this.selEl.querySelector( 'li.cs-selected' );
      		if( oldOpt ) {
      			classie.remove( oldOpt, 'cs-selected' );
      		}
      		classie.add( opt, 'cs-selected' );

      		// if there´s a link defined
      		if( opt.getAttribute( 'data-link' ) ) {
      			// open in new tab?
      			if( this.options.newTab ) {
      				window.open( opt.getAttribute( 'data-link' ), '_blank' );
      			}
      			else {
      				window.location = opt.getAttribute( 'data-link' );
      			}
      		}

      		// callback
      		this.options.onChange( this.el.value );
      	};

      	/**
      	 * returns true if select element is opened
      	 */
      	SelectFx.prototype._isOpen = function(opt) {
      		return classie.has( this.selEl, 'cs-active' );
      	};

      	/**
      	 * removes the focus class from the option
      	 */
      	SelectFx.prototype._removeFocus = function(opt) {
      		var focusEl = this.selEl.querySelector( 'li.cs-focus' );
      		if( focusEl ) {
      			classie.remove( focusEl, 'cs-focus' );
      		}
      	};

      	return SelectFx;

      }

      	if ( typeof define === 'function' && define.amd ) {
      		// AMD
      		define( ["classie"], factory );
      	} else {
      		// add to global namespace
      		window.SelectFx = factory(window.classie);
      	}

      } )();

(function() {
  [].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {
    new SelectFx(el);
  } );
})();
var Modal = (function() {

var trigger = $qsa('.modal-trigger'); // what you click to activate the modal
var modals = $qsa('.modal'); // the entire modal (takes up entire window)
var modalsbg = $qsa('.modal-bg'); // the entire modal (takes up entire window)
var content = $qsa('.modal-content'); // the inner content of the modal
var closers = $qsa('.modal-close'); // an element used to close the modal
var w = window;
var isOpen = false;
var contentDelay = 400; // duration after you click the button and wait for the content to show
var len = trigger.length;

// make it easier for yourself by not having to type as much to select an element
function $qsa(el) {
  return document.querySelectorAll(el);
}

var getId = function(event) {

  event.preventDefault();
  var self = this;
  // get the value of the data-modal attribute from the button
  var modalId = self.dataset.modal;
  var len = modalId.length;
  // remove the '#' from the string
  var modalIdTrimmed = modalId.substring(1, len);
  // select the modal we want to activate
  var modal = document.getElementById(modalIdTrimmed);
  // execute function that creates the temporary expanding div
  makeDiv(self, modal);
};

var makeDiv = function(self, modal) {

  var fakediv = document.getElementById('modal__temp');

  /**
   * if there isn't a 'fakediv', create one and append it to the button that was
   * clicked. after that execute the function 'moveTrig' which handles the animations.
   */

  if (fakediv === null) {
    var div = document.createElement('div');
    div.id = 'modal__temp';
    self.appendChild(div);
    moveTrig(self, modal, div);
  }
};

var moveTrig = function(trig, modal, div) {
  var trigProps = trig.getBoundingClientRect();
  var m = modal;
  var mProps = m.querySelector('.modal-content').getBoundingClientRect();
  var transX, transY, scaleX, scaleY;
  var xc = w.innerWidth / 2;
  var yc = w.innerHeight / 2;

  // this class increases z-index value so the button goes overtop the other buttons
  trig.classList.add('modal-trigger--active');

  // these values are used for scale the temporary div to the same size as the modal
  scaleX = mProps.width / trigProps.width;
  scaleY = mProps.height / trigProps.height;

  scaleX = scaleX.toFixed(3); // round to 3 decimal places
  scaleY = scaleY.toFixed(3);


  // these values are used to move the button to the center of the window
  transX = Math.round(xc - trigProps.left - trigProps.width / 2);
  transY = Math.round(yc - trigProps.top - trigProps.height / 2);

  // if the modal is aligned to the top then move the button to the center-y of the modal instead of the window
  if (m.classList.contains('modal--align-top')) {
    transY = Math.round(mProps.height / 2 + mProps.top - trigProps.top - trigProps.height / 2);
  }


  // translate button to center of screen
  trig.style.transform = 'translate(' + transX + 'px, ' + transY + 'px)';
  trig.style.webkitTransform = 'translate(' + transX + 'px, ' + transY + 'px)';
  // expand temporary div to the same size as the modal
  div.style.transform = 'scale(' + scaleX + ',' + scaleY + ')';
  div.style.webkitTransform = 'scale(' + scaleX + ',' + scaleY + ')';


  window.setTimeout(function() {
    window.requestAnimationFrame(function() {
      open(m, div);
    });
  }, contentDelay);

};

var open = function(m, div) {

  if (!isOpen) {
    // select the content inside the modal
    var content = m.querySelector('.modal-content');
    // reveal the modal
    m.classList.add('modal--active');
    // reveal the modal content
    content.classList.add('modal-content--active');

    /**
     * when the modal content is finished transitioning, fadeout the temporary
     * expanding div so when the window resizes it isn't visible ( it doesn't
     * move with the window).
     */

    content.addEventListener('transitionend', hideDiv, false);

    isOpen = true;
  }

  function hideDiv() {
    // fadeout div so that it can't be seen when the window is resized
    div.style.opacity = '0';
    content.removeEventListener('transitionend', hideDiv, false);
  }
};

var close = function(event) {

  event.preventDefault();
  event.stopImmediatePropagation();

  var target = event.target;
  var div = document.getElementById('modal__temp');

  /**
   * make sure the modal-bg or modal-close was clicked, we don't want to be able to click
   * inside the modal and have it close.
   */

  if (isOpen && target.classList.contains('modal-bg') || target.classList.contains('modal-close')) {

    // make the hidden div visible again and remove the transforms so it scales back to its original size
    div.style.opacity = '1';
    div.removeAttribute('style');

    /**
    * iterate through the modals and modal contents and triggers to remove their active classes.
    * remove the inline css from the trigger to move it back into its original position.
    */

    for (var i = 0; i < len; i++) {
      modals[i].classList.remove('modal--active');
      content[i].classList.remove('modal-content--active');
      trigger[i].style.transform = 'none';
      trigger[i].style.webkitTransform = 'none';
      trigger[i].classList.remove('modal-trigger--active');
    }

    // when the temporary div is opacity:1 again, we want to remove it from the dom
    div.addEventListener('transitionend', removeDiv, false);

    isOpen = false;

  }

  function removeDiv() {
    setTimeout(function() {
      window.requestAnimationFrame(function() {
        // remove the temp div from the dom with a slight delay so the animation looks good
        div.remove();
      });
    }, contentDelay - 50);
  }

};

var bindActions = function() {
  for (var i = 0; i < len; i++) {
    trigger[i].addEventListener('click', getId, false);
    closers[i].addEventListener('click', close, false);
    modalsbg[i].addEventListener('click', close, false);
  }
};

var init = function() {
  bindActions();
};

return {
  init: init
};

}());

Modal.init();


$(".fab-btn").on('click', function(e) {
  $(".fab-ele").removeClass("no");
  if(e.target != this) return;
  $('.fab-btn, .fab-ele').toggleClass("active-up");
});

// tabs js starts from here
(function() {

  'use strict';

  /**
   * tabs
   *
   * @description The Tabs component.
   * @param {Object} options The options hash
   */
  var tabs = function(options) {

    var el = document.querySelector(options.el);
    var tabNavigationLinks = el.querySelectorAll(options.tabNavigationLinks);
    var tabContentContainers = el.querySelectorAll(options.tabContentContainers);
    var activeIndex = 0;
    var initCalled = false;

    /**
     * init
     *
     * @description Initializes the component by removing the no-js class from
     *   the component, and attaching event listeners to each of the nav items.
     *   Returns nothing.
     */
    var init = function() {
      if (!initCalled) {
        initCalled = true;
        el.classList.remove('no-js');

        for (var i = 0; i < tabNavigationLinks.length; i++) {
          var link = tabNavigationLinks[i];
          handleClick(link, i);
        }
      }
    };

    /**
     * handleClick
     *
     * @description Handles click event listeners on each of the links in the
     *   tab navigation. Returns nothing.
     * @param {HTMLElement} link The link to listen for events on
     * @param {Number} index The index of that link
     */
    var handleClick = function(link, index) {
      link.addEventListener('click', function(e) {
        e.preventDefault();
        goToTab(index);
      });
    };

    /**
     * goToTab
     *
     * @description Goes to a specific tab based on index. Returns nothing.
     * @param {Number} index The index of the tab to go to
     */
    var goToTab = function(index) {
      if (index !== activeIndex && index >= 0 && index <= tabNavigationLinks.length) {
        tabNavigationLinks[activeIndex].classList.remove('is-active');
        tabNavigationLinks[index].classList.add('is-active');
        tabContentContainers[activeIndex].classList.remove('is-active');
        tabContentContainers[index].classList.add('is-active');
        activeIndex = index;
      }
    };

    /**
     * Returns init and goToTab
     */
    return {
      init: init,
      goToTab: goToTab
    };

  };

  /**
   * Attach to global namespace
   */
  window.tabs = tabs;

})();
//
// var myTabs = tabs({
//     el: '#tabs',
//     tabNavigationLinks: '.tabs-nav-link',
//     tabContentContainers: '.tab'
//   });
//   myTabs.init();
  (function(){!function(e){"use strict";e.fn.paperCollapse=function(o){var a;return a=e.extend({},e.fn.paperCollapse.defaults,o),e(this).find(".collapse-card-heading").add(a.closeHandler).click(function(){e(this).closest(".collapse-card").hasClass("active")?(a.onHide.call(this),e(this).closest(".collapse-card").removeClass("active"),e(this).closest(".collapse-card").find(".collapse-card-body").slideUp(a.animationDuration,a.onHideComplete)):(a.onShow.call(this),e(this).closest(".collapse-card").addClass("active"),e(this).closest(".collapse-card").find(".collapse-card-body").slideDown(a.animationDuration,a.onShowComplete))}),this},e.fn.paperCollapse.defaults={animationDuration:400,easing:"swing",closeHandler:".collapse-card-close",onShow:function(){},onHide:function(){},onShowComplete:function(){},onHideComplete:function(){}}}(jQuery)}).call(this);
   $('.collapse-card').paperCollapse();
   // The plugin code
   (function($){
     $.fn.parallax = function(options){
       var $$ = $(this);
       offset = $$.offset();
       var defaults = {
         'start': 0,
         'stop': offset.top + $$.height(),
         'coeff': 0.95
       };
       var opts = $.extend(defaults, options);
       return this.each(function(){
         $(window).bind('scroll', function() {
           windowTop = $(window).scrollTop();
           if((windowTop >= opts.start) && (windowTop <= opts.stop)) {
             newCoord = windowTop * opts.coeff;
             $$.css({
                 'background-position': '0 '+ newCoord + 'px'
             });
           }
         });
       });
     };
   })(jQuery);
