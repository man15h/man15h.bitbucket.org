var app = angular.module('app', ['ngMaterial','ngAnimate','ngRoute','hljs']);
app.constant("$MD_THEME_CSS","");
app.config(['$routeProvider',
  function ($routeProvider) {
        $routeProvider
        .when('/', {
            title: 'Home',
            templateUrl: 'partials/introduction.html',
            controller: 'myCtrl'
        })
        .when('/grid', {
            title: 'Grid',
            templateUrl: 'partials/grid.html',
            controller: 'myCtrl'
        })

        .when('/templates', {
            title: 'Templates',
            templateUrl: 'partials/templates.html',
            controller: 'myCtrl'
        })
        .when('/helper', {
            title: 'Helper',
            templateUrl: 'partials/helpers.html',
            controller: 'myCtrl'
        })
        .when('/cards', {
            title: 'Cards',
            templateUrl: 'partials/cards.html',
            controller: 'myCtrl'
        })
        .when('/lists', {
              title: 'Lists',
              templateUrl: 'partials/lists.html',
              controller: 'myCtrl'
          })
        .when('/table', {
            title: 'Table',
            templateUrl: 'partials/table.html',
            controller: 'myCtrl'
        })
        .when('/chips', {
            title: 'Chips',
            templateUrl: 'partials/chips.html',
            controller: 'myCtrl'
        })
        .when('/forms', {
            title: 'Forms',
            templateUrl: 'partials/forms.html',
            controller: 'myCtrl'
        })
        .when('/chips', {
            title: 'Chips',
            templateUrl: 'partials/chips.html',
            controller: 'myCtrl'
        })
        .when('/buttons', {
            title: 'Buttons',
            templateUrl: 'partials/buttons.html',
            controller: 'myCtrl'
        })
        .when('/icons', {
            title: 'Icons',
            templateUrl: 'partials/icons.html',
            controller: 'myCtrl'
        })
        .when('/preloader', {
            title: 'Preloader',
            templateUrl: 'partials/preloader.html',
            controller: 'myCtrl'
        })
        .when('/breadcrumbs', {
            title: 'Breadcrumbs',
            templateUrl: 'partials/breadcrumbs.html',
            controller: 'myCtrl'
        })
        .when('/dropdown', {
            title: 'Dropdown',
            templateUrl: 'partials/dropdown.html',
            controller: 'myCtrl'
        })
        .when('/fab', {
            title: 'FAB',
            templateUrl: 'partials/fab.html',
            controller: 'myCtrl'
        })
        .when('/tabs', {
            title: 'Tabs',
            templateUrl: 'partials/tabs.html',
            controller: 'myCtrl'
        })
        .when('/collapsible', {
            title: 'collapsible',
            templateUrl: 'partials/collapsible.html',
            controller: 'myCtrl'
        })
        .when('/tooltip', {
            title: 'Tooltip',
            templateUrl: 'partials/tooltip.html',
            controller: 'myCtrl'
        })
        .when('/navbar', {
            title: 'Navbar',
            templateUrl: 'partials/navbar.html',
            controller: 'myCtrl'
        })
        .when('/parallax', {
            title: 'Navbar',
            templateUrl: 'partials/parallax.html',
            controller: 'myCtrl'
        })
        .when('/modals', {
            title: 'Modals',
            templateUrl: 'partials/modals.html',
            controller: 'myCtrl'
        })
        .when('/release', {
            title: 'Release',
            templateUrl: 'partials/release.html',
            controller: 'myCtrl'
        })
        .when('/typography', {
            title: 'Release',
            templateUrl: 'partials/typography.html',
            controller: 'myCtrl'
        })
        .when('/colors', {
            title: 'colors',
            templateUrl: 'partials/colors.html',
            controller: 'myCtrl'
        })
        .otherwise({
            redirectTo: '/'
        });
  }])
app.config(function (hljsServiceProvider) {
hljsServiceProvider.setOptions({
  // replace tab with 4 spaces
  tabReplace: '    '
});
});
app.controller('myCtrl', function($scope,$location) {
  $scope.isActive = function(route) {
      return route === $location.path();
      console.log(route);
    }
  // $scope.$on('$viewContentLoaded', addCrudControls);
});
