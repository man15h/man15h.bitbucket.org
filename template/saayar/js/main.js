var app = angular.module('app', ['ngRoute','duScroll','angular-scroll-animate','ngMaterial','ngAnimate']);
app.config(['$routeProvider',
  function ($routeProvider) {
      $routeProvider
      .when('/', {
          title: 'Home',
          templateUrl: 'template/home.html',
          controller: 'myCtrl'
      })
      .when('/biography', {
          title: 'Home',
          templateUrl: 'template/biography.html',
          controller: 'myCtrl'
      })
      .when('/poetry', {
          title: 'Home',
          templateUrl: 'template/poetry.html',
          controller: 'myCtrl'
      })
      .when('/posts', {
          title: 'Home',
          templateUrl: 'template/published-posts.html',
          controller: 'myCtrl'
      })
      .when('/bookshow', {
          title: 'Home',
          templateUrl: 'template/book.html',
          controller: 'myCtrl'
      })

      .when('/photos', {
          title: 'Gallery',
          templateUrl: 'template/gallery.html',
          controller: 'myCtrl'
      })
      .otherwise({
          redirectTo: '/'
      });
      // $locationProvider.html5Mode(true);

  }])
app.controller('myCtrl', function($scope, $location, $http, $timeout) {
  $scope.isPath= function(viewLocation) {
    return viewLocation === $location.path();
  };
  $scope.loading=true;
  $scope.isMenuActive=false
  $scope.modalimg=[];
  $scope.toggleMenu=function(){
    $scope.isMenuActive=!$scope.isMenuActive
  };
  $scope.isImgBig=false;
  $scope.expandImg= function($index,img){
    if($index==0){
      $scope.hidePrevImg=true;
      $scope.hideNextImg=false;
    }
    else if($index==img.length-1){
      $scope.hideNextImg=true;
      $scope.hidePrevImg=false;
    }
    else{
      $scope.hideNextImg=false;
      $scope.hidePrevImg=false;
    }
    $scope.modalimg.url=img[$index].src;
    $scope.modalimg.index=$index;
    $scope.isImgBig=!$scope.isImgBig;
  }
  $scope.deExpandImg=function () {
    $scope.isImgBig=!$scope.isImgBig;
  }
  $scope.animation = {};
  $scope.animation.current = 'fadeInUp';
  $scope.animation.previous = $scope.animation.current;
  $scope.ElementIn = function($el) {
		$el.removeClass('not-visible');
		$el.addClass('animated ' + $scope.animation.current);
	};
	$scope.animateElementOut = function($el) {
		$el.addClass('not-visible');
		$el.removeClass('animated ' + $scope.animation.current);
	};
  $scope.user=null;
  function getPoem(){
    $http({
         method: 'GET',
         url: 'js/poem.json'
      }).then(function (success){
        $scope.biography=success.data.biography;
        $scope.poem=success.data.poem;
        $scope.ghazals=success.data.ghazals;
        $scope.images=success.data.images;
      },function (error){

    });
  };
  $scope.nextImg=function (im) {
    console.log(im+' and '+$scope.images.length);
    if (im==$scope.images.length-2){
      $scope.hideNextImg=true;
    }
    else{
      $scope.hidePrevImg=false;
      $scope.hideNextImg=false;
    }
    $scope.modalimg.url=  $scope.images[im+1].src;
    $scope.modalimg.index=im+1;
  }
  $scope.prevImg=function (im) {
    $scope.modalimg.url=  $scope.images[im-1].src;
    $scope.modalimg.index=im-1;
    if(im==0){
      $scope.hidePrevImg=true;
    }
    else{
      $scope.hideNextImg=false;
      $scope.hidePrevImg=false;
    }
  }
$scope.send=function (user){
  $timeout(function(){$scope.user.name=null;
  $scope.user.email=null;
  $scope.user.message=null;
  $scope.mailResponse="Mail successfully delivered, we will get to you soon"}, 2000);
  $.ajax({
             url: "../php/mail.php",
             type: "POST",
             data: {
                 name: user.name,
                 phone: user.email,
                 email: user.email,
                 message: user.message
             },
             cache: false}).then(function (success){
               $scope.user.name=null;
               $scope.user.email=null;
               $scope.user.message=null;
               $scope.mailResponse=success;
               console.log(success);
             },function (error){
               $scope.mailResponse=error;
               console.log(error.responseText);
           });
      };
  $timeout(function(){$scope.loading = false}, 2000);
  getPoem();


});
